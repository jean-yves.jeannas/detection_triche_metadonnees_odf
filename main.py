#!/usr/bin/env python
# Python program to create
# a file explorer in Tkinter
#test modif 

# import all components
# from the tkinter library
from tkinter import *

# import filedialog module
from tkinter import filedialog

import os
import sys

from pathlib import Path
from odfdo import Document
import math

dossier = "/home/jean-yves/Documents/travail/UdLille/CCN/cours/"
mondossier = "."
ledossier = "."

# Function for opening the
# file explorer window
def browseFiles():
    os.chdir(dossier)
    ledossier = filedialog.askdirectory()
    os.chdir(ledossier)
    print("===")
    print("======")
    print (dossier)
    print("============")
    print("================")
    print(ledossier)
    print("======================")

    os.system ("echo debut_analyse_metadonnees > meta3.txt")
    os.system("echo " + repr(ledossier) + " >> meta3.txt")



    for subdir, dirs, files in os.walk(ledossier):
        for file in files:
            try:
                filepath = subdir + os.sep + file

                if filepath.endswith(".odt") or filepath.endswith(".ods") or filepath.endswith(".odp"):
                    print (filepath)
                    document = Document(filepath)
                    meta = document.get_part("meta.xml")
                    print("editing duration     :", meta.get_editing_duration())
                    if (meta.get_editing_duration() == None) :
                        print ("BUGGGGGGGGGGGGG!!!!!!!!!!!!!!!!!!!")
                        print ("+++++++++++++++++++++++++++++++++++++++++++++")
                        os.system("echo ++++++++++++++++++++++++++++++++++++++++++ >> meta3.txt")
                        os.system("echo " + repr(filepath) + " >> meta3.txt")
                        os.system("echo PAS_DE_DONNEES " + repr(minutes) + "  >> meta3.txt")
                    else:
                        duree_edition = meta.get_editing_duration().total_seconds()
                        print ("secondes : ")
                        print (duree_edition)
                        minutes = duree_edition / 60
                        print ("minutes : ")
                        print (minutes)
                        print (minutes < 10.0)
                        if (minutes < 10.0) :
                            print("ATTENTION")
                            os.system ("echo ----------------------------------- >> meta3.txt")
                            os.system("echo " + repr(filepath) + " >> meta3.txt")
                            os.system("echo ATTENTION " + repr(minutes) + "  >> meta3.txt")
                            zonet.insert(INSERT, repr(filepath)+" : "+repr(minutes)+"\n")
                            zonet.pack()

                        print("----------------------------------------------------------------------------------------------")
            except KeyError:
                print("******************* Loupé ! ********************")
                os.system("echo ========================= >> meta3.txt")
                os.system("echo " + repr(filepath) + " >> meta3.txt")
                os.system("echo \nATTENTION PAS DE META\n >> meta3.txt")
                zonet.insert(INSERT, repr(filepath) + " Pas de méta\n")
                zonet.pack()


# Create the root window
window = Tk()

# Set window title
window.title('Analyse Métadonnées fichiers ODF')

# Set window size
window.geometry("800x300")

#Set window background color
window.config(background = "white")

# Create a File Explorer label
label_file_explorer = Label(window,
                            text = "Détecteur de triche pour fichiers ODF",
                            width = 100, height = 4,
                            fg = "blue")


button_explore = Button(window,
                        text = "Choix dossier",
                        command = browseFiles)

#button_liste = Button(window,
#                      text = "Liste",
#                      command = listedossier(dossier))

button_exit = Button(window,
                     text = "Exit",
                     command = exit)

# Grid method is chosen for placing
# the widgets at respective positions
# in a table like structure by
# specifying rows and columns
label_file_explorer.grid(column = 1, row = 1)

button_explore.grid(column = 1, row = 2)

button_exit.grid(column = 1,row = 3)

#button_liste.grid(column = 1, row = 4)

fen = Tk()
fen.geometry("1200x400")
fen.title('Fichiers à contrôler (édition < 10 mn) :')
fen.attributes('-topmost',1)
zonet = Text(fen, height=400, width=1200)
zonet.insert (INSERT, "Début :\n")
zonet.pack()

# Let the window wait for any events
window.mainloop()
